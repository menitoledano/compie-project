import {DataModel} from "./data.model";
import {EventEmitter, Injectable} from "@angular/core";
import {HttpService} from "./http.service";
import {Response} from '@angular/http';


@Injectable()
export class AppDataService {
    public data: DataModel[] = [];
    public dataFetched = new EventEmitter<boolean>();
    public onChoosingSong = new EventEmitter<number>();
    public onClickQuote = new EventEmitter<number>();

    constructor(private httpService: HttpService) {
    }


    initiateData(): void {
        this.getData();
    }


    getData() {
        return this.httpService.getSongs().subscribe(
            (response: Response) => {
                const dataJson = response.json();
                if (dataJson.length > 0) {
                    for (let i = 0; i < dataJson.length; i++) {
                        const tempNode: DataModel = dataJson[i];
                        this.data.push(tempNode);
                    }
                    this.dataFetched.emit(true);
                }
            },
            (error) => {
                throw new Error('ERROR')
            }
        );
    }
}