import {Injectable} from "@angular/core";
import {Http} from '@angular/http';

@Injectable()
export class HttpService {

    BASE_URL: string = 'https://glacial-escarpment-40412.herokuapp.com/songs';
    tempUrlString: string;

    constructor(private http: Http) {
    }

    getSongs() {
        this.tempUrlString = this.BASE_URL;
        return this.http.get(this.tempUrlString);
    }
}