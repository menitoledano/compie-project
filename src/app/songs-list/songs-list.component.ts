import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {HttpService} from "../http.service";
import {AppDataService} from "../app-data.service";


@Component({
    selector: 'app-songs-list',
    templateUrl: './songs-list.component.html',
    styleUrls: ['./songs-list.component.css']
})
export class SongsListComponent implements OnInit {

    private songs: string[] = [];
    @Output() private chosenSong = new EventEmitter<number>();
    private matchSongs: string[] = [];

    constructor(private httpService: HttpService,
                private appDataService: AppDataService) {
    }


    ngOnInit() {
        this.appDataService.dataFetched.subscribe(
            (dataFetched: boolean) => {
                this.fetchArtists();
            }
        )
    }

    fetchArtists(): void {
        this.songs = [];
        for (let i = 0; i < this.appDataService.data.length; i++)
            this.songs.push(this.appDataService.data[i].title);
    }


    onClickSong(index: number): void {
        this.appDataService.onChoosingSong.emit(index);
    }


    searchSong(substring: string): void {
        if (!substring) {
            this.fetchArtists();
        } else {
            this.matchSongs = [];
            for (let i = 0; i < this.songs.length; i++) {
                let song = this.songs[i].toLocaleLowerCase();
                if (song.includes(substring.toLocaleLowerCase())) {
                    this.matchSongs.push(this.songs[i])
                }
            }
            this.songs = this.matchSongs;
        }
    }


}
