export class DataModel {
    constructor(id: number, title: string, youtubeId: string, quotes: { text: string, offset: number }[]) {
        this.id = id;
        this.title = title;
        this.youtubeId = youtubeId;
        this.quotes = quotes;
    }

    public id: number;
    public title: string;
    public youtubeId: string;
    public quotes: { text: string, offset: number }[];
}

