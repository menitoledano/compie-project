import {Component, OnInit} from '@angular/core';
import {AppDataService} from "./app-data.service";
import {HttpService} from "./http.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    private songName: string;

    constructor(private appDataService: AppDataService,
                private httpService: HttpService) {
    }

    ngOnInit() {
        this.fillData();

    }

    fillData(): void {
       this.appDataService.initiateData();
    };


    onChosingSong(song: string): void {
        this.songName = song;
    }

}
