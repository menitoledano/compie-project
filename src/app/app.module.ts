import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from "@angular/http";

import {AppComponent} from './app.component';
import {HttpService} from "./http.service";
import {SongsListComponent} from './songs-list/songs-list.component';
import {YoutubePlayerComponent} from './youtube-player/youtube-player.component';
import {NgxYoutubePlayerModule} from "ngx-youtube-player";
import {QuotesComponent} from './youtube-player/quotes/quotes.component';
import {AppDataService} from "./app-data.service";
import {PlayerComponent} from './youtube-player/player/player.component';


@NgModule({
    declarations: [
        AppComponent,
        SongsListComponent,
        YoutubePlayerComponent,
        QuotesComponent,
        PlayerComponent,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        NgxYoutubePlayerModule.forRoot()
    ],
    providers: [HttpService, AppDataService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
