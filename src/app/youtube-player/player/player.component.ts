import {Component, Input, OnInit} from '@angular/core';
import {AppDataService} from "../../app-data.service";

@Component({
    selector: 'app-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

    @Input() private id: string;
    private player;
    private ytEvent;

    constructor(private appDataService: AppDataService) {
    }



    ngOnInit() {
        this.appDataService.onChoosingSong.subscribe(
            (index: number) => {
                this.id = this.appDataService.data[index].youtubeId;
                this.player.cueVideoById(this.id);
                this.player.nextVideo();
                this.player.playVideo();

            }
        );


        this.appDataService.onClickQuote.subscribe(
            (offset: number) => {
                this.player.seekTo(offset, true);
            }
        );
    }


    onStateChange(event): void {
        this.ytEvent = event.data;
    }

    savePlayer(player): void {
        this.player = player;
    }

    playVideo(): void {
        this.player.playVideo();

    }

    pauseVideo(): void {
        this.player.pauseVideo();
    }


}
