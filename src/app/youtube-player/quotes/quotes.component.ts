import {Component, Input} from '@angular/core';
import {AppDataService} from "../../app-data.service";

@Component({
    selector: 'app-quotes',
    templateUrl: './quotes.component.html',
    styleUrls: ['./quotes.component.css']
})
export class QuotesComponent {

    @Input() private quotes: { text: string, offset: number }[];

    constructor(private appDataService: AppDataService) {
    }


    updateOffset(offset: number): void {
        this.appDataService.onClickQuote.emit(offset);
    }
}
