import {Component, OnInit} from '@angular/core';
import {AppDataService} from "../app-data.service";

@Component({
    selector: 'app-youtube-player',
    templateUrl: './youtube-player.component.html',
    styleUrls: ['./youtube-player.component.css']
})
export class YoutubePlayerComponent implements OnInit {

    private songTitle: string;
    private id: string;
    private quotes: { text: string, offset: number }[] = [];


    constructor(private appDataService: AppDataService) {
    }

    ngOnInit() {
        this.songTitle = 'Choose Song From The List';
        this.appDataService.onChoosingSong.subscribe(
            (response: number) => {
                this.setData(response);
            }
        )
    }

    setData(index: number): void {
        this.songTitle = this.appDataService.data[index].title;
        this.id = this.appDataService.data[index].youtubeId;
        this.quotes = this.appDataService.data[index].quotes;
    }


}
